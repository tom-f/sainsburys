<?php

namespace TomF\Sainsburys\Model\Document;

use Symfony\Component\DomCrawler\Crawler;


class SymfonyDomDocument implements DocumentInterface
{
    /**
     * @var Crawler
     */
    private $crawler;

    public function __construct($html)
    {
        $this->crawler = new Crawler($html);
    }

    /**
     * @param $query
     * @return array
     */
    public function getTextFields($query)
    {
        $texts = array();

        try {
            $nodes = $this->get($query);
            if ($nodes->count() > 1) {
                $nodes->each(function ($node) use (&$texts) {
                    /** @var Crawler $node */
                    $texts[] = $node->text();
                });
            } else {
                $texts[] = $nodes->text();
            }

        } catch (\InvalidArgumentException $e) {

            //@todo is there a further requirement here (see assumptions)
            $texts = array();
        }

        return $texts;


    }

    /**
     * @param string $query
     * @return array
     */
    public function getLinks($query)
    {
        $links = array();

        try {
            $nodes = $this->get($query);
            if ($nodes->count() > 1) {
                $nodes->each(function ($node) use (&$links) {
                    /** @var Crawler $node */
                    $links[] = $node->attr('href');
                });
            } else {
                $links[] = $nodes->attr('href');
            }

        } catch (\InvalidArgumentException $e) {
            //@todo is there a further requirement here (see assumptions)
            $links = array();
        }

        return $links;
    }

    /**
     * @param $query
     * @return Crawler
     */
    private function get($query)
    {
        return $this->crawler->filter($query);
    }

}
