<?php

namespace TomF\Sainsburys\Model\Document;


interface DocumentInterface
{

    /**
     * Get list of hrefs of element at query
     *
     * @param string $query
     * @return array
     */
    public function getLinks($query);


    /**
     * Get Text values of elements at query
     *
     * @param $query
     * @return array
     */
    public function getTextFields($query);

}


