<?php

namespace TomF\Sainsburys\Model\Product;

use TomF\Sainsburys\Model\Product;
use TomF\Sainsburys\Model\ToJsonInterface;

class ProductList implements ToJsonInterface
{

    /**
     * @var Product[]
     */
    private $products = array();

    public function addProduct($product)
    {
        $this->products[] = $product;
    }

    /**
     * @todo json_encode($numbers,JSON_PRESERVE_ZERO_FRACTION); might help here in php 5.6
     * @return string
     */
    public function toJson()
    {
        $priceTotal = 0.0;

        $results = "";
        foreach($this->products as $product) {
            $priceTotal += $product->getUinitPrice();
            $results .= $product->toJson() . ',';
        }

        $results = '"results":[' . substr($results, 0, -1) . '],';

        $json = "{" . $results  . '"total":' . number_format($priceTotal, 2)  . "}";

        return $json;

    }

}