<?php

namespace TomF\Sainsburys\Model;


class Product implements ToJsonInterface
{
    private $title = '';
    private $unitPrice = 0;
    private $description = '';
    private $size = '';

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return float
     */
    public function getUinitPrice()
    {
        return (float)$this->unitPrice;
    }

    /**
     * @todo json_encode($numbers,JSON_PRESERVE_ZERO_FRACTION); might help here in php 5.6
     * @return array
     */
    public function toJson()
    {
        $content = array();
        $content['title'] = $this->title;
        $content['size'] = $this->size;
        $content['unit_price'] = number_format($this->unitPrice, 2);
        $content['description'] = $this->description;

        $json = "";
        foreach ($content as $key => $value) {
            $json .= '"' . $key . '":';
            if ($key == 'unit_price') {
                 $json .= $value  . '';
            } else {
                $json .= '"' . str_replace('"', '', $value) . '"';
            }

            $json .= ',';
        }

        $json = "{" . substr($json, 0, -1) . "}";

        return $json;
    }
}