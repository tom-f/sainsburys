<?php

namespace TomF\Sainsburys\Model;

interface ToJsonInterface
{
    /**
     * return json representation of class
     */
    public function toJson();
}