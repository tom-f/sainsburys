<?php

namespace TomF\Sainsburys\HttpClient;

use Psr\Log\LoggerInterface;
use TomF\Sainsburys\Model\Document\SymfonyDomDocument;

class CurlClient implements HttpClientInterface
{

    const HTTP_RESPONSE_OK = 200;

    private $lastResponseCode = null;

    private $lastRequestSize = 0;

    private $logger = null;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * Download given url using curl and create document model from result.
     *
     * @param $url
     * @return bool|SymfonyDomDocument
     */
    public function fetchDocument($url)
    {
        $this->resetDefaultValues();

        $this->log("Request Url: {$url}");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $this->lastResponseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $this->log("Response Code: {$this->lastResponseCode}");
        if ($this->isLastResponseOk()) {
            $this->lastRequestSize = curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            curl_close($curl);



            return new SymfonyDomDocument($response);
        }

        curl_close($curl);
        return false;
    }

    /**
     * Size of last response in bytes
     *
     * @return int
     */
    public function getLastRequestSize()
    {
        return $this->lastRequestSize;
    }

    private function resetDefaultValues()
    {
        $this->lastRequestSize = 0;
        $this->lastResponseCode = null;
    }

    /**
     * Did we get a 200
     *
     * @return bool
     */
    private function isLastResponseOk()
    {
       return (self::HTTP_RESPONSE_OK == $this->lastResponseCode); 
    }

    private function log($message)
    {
        if ($this->logger) {
            $this->logger->info($message);
        }
    }

}
