<?php

namespace TomF\Sainsburys\HttpClient;

use TomF\Sainsburys\Model\Document\DocumentInterface;

interface HttpClientInterface
{
    /**
     * @param $url
     * @return DocumentInterface|false
     */
    public function fetchDocument($url);

    /**
     * Last request size in bytes
     *
     * @return int
     */
    public function getLastRequestSize();
}