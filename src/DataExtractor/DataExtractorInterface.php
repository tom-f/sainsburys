<?php

namespace TomF\Sainsburys\DataExtractor;

interface DataExtractorInterface
{
    public function extract();
}