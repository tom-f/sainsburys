<?php

namespace TomF\Sainsburys\DataExtractor;


use TomF\Sainsburys\HttpClient\HttpClientInterface;
use TomF\Sainsburys\Model\Product;

class ProductDataExtractor implements DataExtractorInterface
{

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    private $url;

    public function __construct(HttpClientInterface $httpClient, $url)
    {
        $this->httpClient = $httpClient;
        $this->url = $url;
    }

    /**
     * Extract product details from page.
     *
     * @return string
     */
    public function extract()
    {
        // load initial url
        $document = $this->httpClient->fetchDocument($this->url);

        if ($document) {

            // get all product links
            $productLinks = $document->getLinks('div.productInfo > h3 > a');

            $productList = new Product\ProductList();

            // Fetch details of all product models
            foreach ($productLinks as $link) {
                $product = $this->extractProductPageData($link);
                $productList->addProduct($product);
            }

            return $productList->toJson();
        } else {
            echo "Page could not be found";
        }

        return "";
    }

    /**
     * Extracts details from the given url and populates product model.
     *
     * @param $link
     * @return Product
     */
    private function extractProductPageData($link)
    {
        $document = $this->httpClient->fetchDocument($link);

        $product = new Product();

        if ($document) {


            $size = $this->httpClient->getLastRequestSize();
            $sizeInKB = round($size/1024, 2) . "kb";
            $product->setSize($sizeInKB);

            $descriptions = $document->getTextFields('div.productText');
            if (count($descriptions) > 0) {
                $product->setDescription(trim($descriptions[0]));
            }

            $title = $document->getTextFields("div.productTitleDescriptionContainer > h1");
            if (count($title) == 1) {
                $product->setTitle($title[0]);

            }

            $price = $document->getTextFields("p.pricePerUnit");

            if (count($price) > 0) {
                // Strip non digits from string
                $priceStringDigits = preg_replace('/[^0-9]/', '', $price[0]);
                // Reformat due to loss of decimal place
                $price = (float)$priceStringDigits/100;

                $product->setUnitPrice($price);
            }
        }

        return $product;
    }

}
