# Requirements
php >= 5.5.27 (though untested on previous versions)

# Running

## Linux/OSX
from the project root:
 - composer install
 - chmod +x cli
 - ./cli <url> (url defaults to http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html)


## Windows
untried

## Docker
@todo

# Testing

- composer install
- ./vendor/bin/phpunit

# Assumptions
- Size and product data should be empty if product link does not return a 200 code, or an element cannot be extracted from page.
- I've avoided exactly matching formatting of spacing in json strings + stripped any " characters from value strings.

# Next steps (in no particular order)
- Caching if appropriate.
- Possible refactor to more up to date/better supported hml parser (document model)
- Confirm JSON formatting, test json_encode($numbers,JSON_PRESERVE_ZERO_FRACTION) on php 5.6
- Explore 'size' requirement further
- Add CI
- Docker for testing different php versions.
- Add abstraction layer around 'document queries', some kind of query converter
- Add some kind of model for 'node returns' in document model, include better handling for unfound nodes
- Expand logging, better config, log more, add identifiers to easy 'match' parts from the same flow