<?php

use TomF\Sainsburys\HttpClient\CurlClient;

class CurlClientTest extends PHPUnit_Framework_TestCase 
{

    public function testResponseStatus()
    {
        $httpClient = new CurlClient();   
        $document = $httpClient->fetchDocument("http://www.tomf.co.uk");

        $this->assertEquals(4483, $httpClient->getLastRequestSize());

        $this->assertInstanceOf('TomF\Sainsburys\Model\Document\SymfonyDomDocument',$document);

        $this->assertFalse($httpClient->fetchDocument("http://www.tomf.co.uk/non-exists"));

    }

}
