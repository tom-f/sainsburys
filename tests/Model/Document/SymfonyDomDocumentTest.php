<?php

use \TomF\Sainsburys\Model\Document\SymfonyDomDocument;

class SymfonyDomDocumentTest extends PHPUnit_Framework_TestCase
{

    public function testGetLink()
    {
        $document = $this->getSingleNodeDocument();
        $links = $document->getLinks('div.productInfo > h3 > a');

        $this->assertCount(1, $links);
        $this->assertEquals('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html', $links[0]);
    }

    public function testGetLinkList()
    {
        $document = $this->getMultiNodeDocument();
        $links = $document->getLinks('div.productInfo > h3 > a');
        $this->assertCount(2, $links);
        $this->assertEquals("url1", $links[0]);
        $this->assertEquals("url2", $links[1]);
    }

    public function testGetTextFields()
    {
        $textFields = $this->getSingleNodeDocument()->getTextFields("div.productInfo > h3 > a");

        $this->assertCount(1, $textFields);
        $this->assertEquals("link text", $textFields[0]);
    }

    public function testNodeNotFound()
    {
        $document = $this->getSingleNodeDocument();
        $this->assertEquals(array(), $document->getTextFields("b"));
        $this->assertEquals(array(), $document->getLinks("div.i-dont-exist > a"));
    }

    /**
     * @return SymfonyDomDocument
     */
    private function getSingleNodeDocument()
    {
        $document = new SymfonyDomDocument("
            <html>
              <body>
                <div class=\"product \">
	              <div class=\"productInner\">
	                <div class=\"productInfoWrapper\">
	                  <div class=\"productInfo\">
                        <h3>
	                      <a href=\"http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html\">link text</a>
	                    </h3>
	                  </div>
	                </div>
	              </div>
	            </div>
              </body>
            </html>
        ");
        return $document;
    }

    /**
     * @return SymfonyDomDocument
     */
    private function getMultiNodeDocument()
    {
        $document = new SymfonyDomDocument("
            <html>
              <body>
                <div class=\"product \">
	              <div class=\"productInner\">
	                <div class=\"productInfoWrapper\">
	                  <div class=\"productInfo\">
                        <h3>
	                      <a href=\"url1\">link text</a>
	                    </h3>
	                  </div>
	                </div>
	              </div>
	            </div>
	            <div class=\"product \">
	              <div class=\"productInner\">
	                <div class=\"productInfoWrapper\">
	                  <div class=\"productInfo\">
                        <h3>
	                      <a href=\"url2\">link text</a>
	                    </h3>
	                  </div>
	                </div>
	              </div>
	            </div>
              </body>
            </html>
        ");
        return $document;
    }


}