<?php

use TomF\Sainsburys\Model\Product;

class ProductTest extends PHPUnit_Framework_TestCase
{

    public function testToJson()
    {
        $parts = array();
        $parts['title'] = 'product title';
        $parts['size'] = '17kb';
        $parts['unit_price'] = (float) 12.50;
        $parts['description'] = 'product description';

        $product = new Product();

        $product->setDescription($parts['description']);
        $product->setTitle($parts['title']);
        $product->setSize($parts['size']);
        $product->setUnitPrice($parts['unit_price']);

        $expectedJson = '{"title":"product title","size":"17kb","unit_price":12.50,"description":"product description"}';
        $this->assertEquals($expectedJson, $product->toJson());
    }

}
