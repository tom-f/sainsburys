<?php

use TomF\Sainsburys\Model\Product\ProductList;
use TomF\Sainsburys\Model\Product;

class ProductListTest extends PHPUnit_Framework_TestCase
{

    public function testToJson()
    {
        $product1 = new Product();
        $product1->setDescription('desc1');
        $product1->setTitle('title1');
        $product1->setSize('12kb');
        $product1->setUnitPrice(5.30);

        $product2 = new Product();
        $product2->setDescription('desc2');
        $product2->setTitle('title1');
        $product2->setSize('12kb');
        $product2->setUnitPrice(7.25);

        $productList = new ProductList();
        $productList->addProduct($product1);
        $productList->addProduct($product2);

        $expectedJson = '{"results":[{"title":"title1","size":"12kb","unit_price":5.30,"description":"desc1"},{"title":"title1","size":"12kb","unit_price":7.25,"description":"desc2"}],"total":12.55}';
        $this->assertEquals($expectedJson, $productList->toJson());
    }
}