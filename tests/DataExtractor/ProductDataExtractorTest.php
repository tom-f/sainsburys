<?php

use TomF\Sainsburys\DataExtractor\ProductDataExtractor;

class ProductDataExtractorTest extends PHPUnit_Framework_TestCase 
{

    public function testExtract()
    {
        /** @var \TomF\Sainsburys\HttpClient\CurlClient $mockHttpClient */
        $mockHttpClient = $this->createMockHttpClient();
        $url = 'inputUrl';
        $dataExtractor = new ProductDataExtractor($mockHttpClient, $url);

        $expectedJson = '{"results":[{"title":"Sainsbury\'s Avocado Ripe & Ready XL Loose 300g","size":"34.31kb","unit_price":1.50,"description":"Avocados"}],"total":1.50}';

        $this->assertEquals($expectedJson, $dataExtractor->extract());
    }

    private function createMockHttpClient()
    {
        $mockHttpClient = $this->getMockBuilder('\TomF\Sainsburys\HttpClient\CurlClient')->disableOriginalConstructor()->getMock();

        $mockHttpClient->expects($this->at(0))
                       ->method('fetchDocument')
                       ->with('inputUrl')
                       ->will($this->returnValue($this->getInitialPageDocument()));

        $mockHttpClient->expects($this->any())
                       ->method('getLastRequestSize')
                       ->will($this->returnValue(35129));

        $mockHttpClient->expects($this->at(1))
                       ->method('fetchDocument')
                       ->with('url1')
                       ->will($this->returnValue($this->getProductPageDocument()));

        return $mockHttpClient;
    }

    private function getInitialPageDocument()
    {
        return new \TomF\Sainsburys\Model\Document\SymfonyDomDocument($this->getInitialPageHtml());
    }

    private function getProductPageDocument()
    {
        return new \TomF\Sainsburys\Model\Document\SymfonyDomDocument($this->getProductHtmlSnippet());
    }

    private function getInitialPageHtml()
    {
        return "<html>
              <body>
                <div class=\"product \">
	              <div class=\"productInner\">
	                <div class=\"productInfoWrapper\">
	                  <div class=\"productInfo\">
                        <h3>
	                      <a href=\"url1\">link text</a>
	                    </h3>
	                  </div>
	                </div>
	              </div>
	            </div></body></html>";
    }

    private function getProductHtmlSnippet()
    {
        return "<div class=\"section productContent\">
              <!-- BEGIN MessageDisplay.jspf --><!-- END MessageDisplay.jspf -->
              <div class=\"errorBanner hidden\" id=\"error572163\"></div>
              <!-- BEGIN CachedProductOnlyDisplay.jsp -->


<div class=\"pdp\">


    <div class=\"productSummary\">
        <div class=\"productTitleDescriptionContainer\">
            <h1>Sainsbury's Avocado Ripe &amp; Ready XL Loose 300g</h1>

            <div id=\"productImageHolder\">
	             <img src=\"http://www.sainsburys.co.uk/wcsstore7.11.1.161/ExtendedSitesCatalogAssetStore/images/catalog/productImages/51/0000000202251/0000000202251_L.jpeg\" alt=\"Image for Sainsbury's Avocado Ripe &amp; Ready XL Loose 300g from Sainsbury's\" class=\"productImage\" id=\"productImageID\">
            </div>



            <div class=\"reviews\">
				<!-- BEGIN CatalogEntryRatingsReviewsInfoDetailsPage.jspf --><!-- END CatalogEntryRatingsReviewsInfoDetailsPage.jspf -->
            </div>
        </div>


	   <div class=\"addToTrolleytabBox\">
	        <!-- Start UserSubscribedOrNot.jspf --><!-- Start UserSubscribedOrNot.jsp --><!--
			If the user is not logged in, render this opening
			DIV adding an addtional class to fix the border top which is removed
			and replaced by the tabs
		-->
		<div class=\"addToTrolleytabContainer addItemBorderTop\">
	<!-- End AddToSubscriptionList.jsp --><!-- End AddSubscriptionList.jspf --><!--
		    ATTENTION!!!
		    <div class=\"addToTrolleytabContainer\">
		    This opening div is inside \"../../ReusableObjects/UserSubscribedOrNot.jsp\"
		    -->
	        <div class=\"pricingAndTrolleyOptions\">


	        <div class=\"priceTab activeContainer priceTabContainer\" id=\"addItem_572163\"> <!-- CachedProductOnlyDisplay.jsp -->
	            <div class=\"pricing\">

<p class=\"pricePerUnit\">
£1.50<abbr title=\"per\">/</abbr><abbr title=\"unit\"><span class=\"pricePerUnitUnit\">unit</span></abbr>
</p>

    <p class=\"pricePerMeasure\">£1.50<abbr title=\"per\">/</abbr><abbr title=\"each\"><span class=\"pricePerMeasureMeasure\">ea</span></abbr>
    </p>

	            </div>

	            <div class=\"addToTrolleyForm \">

<form class=\"addToTrolleyForm\" name=\"OrderItemAddForm_572163\" action=\"OrderItemAdd\" method=\"post\" id=\"OrderItemAddForm_572163\">
    <input type=\"hidden\" name=\"storeId\" value=\"10151\">
    <input type=\"hidden\" name=\"langId\" value=\"44\">
    <input type=\"hidden\" name=\"catalogId\" value=\"10122\">
    <input type=\"hidden\" name=\"URL\" value=\"http://www.sainsburys.co.uk/webapp/wcs/stores/servlet//ProductDisplay?catalogId=10122&amp;level=2&amp;errorViewName=ProductDisplayErrorView&amp;langId=44&amp;categoryId=185749&amp;productId=572162&amp;storeId=10151\">
    <input type=\"hidden\" name=\"errorViewName\" value=\"ProductDisplayView\">
    <input type=\"hidden\" name=\"SKU_ID\" value=\"7678882\">

        <label class=\"access\" for=\"quantity_572162\">Quantity</label>

	        <input name=\"quantity\" id=\"quantity_572162\" type=\"text\" size=\"3\" value=\"1\" class=\"quantity\">


        <input type=\"hidden\" name=\"catEntryId\" value=\"572163\">
        <input type=\"hidden\" name=\"productId\" value=\"572162\">

    <input type=\"hidden\" name=\"page\" value=\"\">
    <input type=\"hidden\" name=\"contractId\" value=\"\">
    <input type=\"hidden\" name=\"calculateOrder\" value=\"1\">
    <input type=\"hidden\" name=\"calculationUsage\" value=\"-1,-2,-3\">
    <input type=\"hidden\" name=\"updateable\" value=\"1\">
    <input type=\"hidden\" name=\"merge\" value=\"***\">

   	<input type=\"hidden\" name=\"callAjax\" value=\"false\">

         <input class=\"button process\" type=\"submit\" name=\"Add\" value=\"Add\">

</form>

	    <div class=\"numberInTrolley hidden numberInTrolley_572163\" id=\"numberInTrolley_572163\">

	    </div>

	            </div>

	        </div>
	        <!-- Start AddToSubscriptionList.jspf --><!-- Start AddToSubscriptionList.jsp --><!-- End AddToSubscriptionList.jsp --><!-- End AddToSubscriptionList.jspf -->
            </div><!-- End pricingAndTrolleyOptions -->
        </div><!-- End addToTrolleytabContainer -->
    </div>

	<div class=\"BadgesContainer\">

		<div class=\"roundelContainer\">

		 </div>

    </div>


    <div id=\"sitecatalyst_ESPOT_NAME_WF_013_eSpot_1\" class=\"siteCatalystTag\">WF_013_eSpot_1</div>

</div>



</div>
<div class=\"mainProductInfoWrapper\">
    <div class=\"mainProductInfo\">
        <p class=\"itemCode\">
            Item code: 7678882
        </p>

        <div class=\"socialLinks\">
        <h2 class=\"access\">Social Links (may open in a new window)</h2>

           <ul>

               <li class=\"twitter\"><a href=\"https://twitter.com/share?text=Check this out&amp;url=http://www.sainsburys.co.uk/shop/gb/groceries/sainsburys-avocado-xl-pinkerton-loose-300g\" target=\"_blank\" onclick=\"s_objectID=&quot;https://twitter.com/share?text=Check%20this%20out&amp;url=http://www.sainsburys.co.uk/shop/gb/groceri_1&quot;;return this.s_oc?this.s_oc(e):true\"><span>Tweet</span> <span class=\"access\">on Twitter</span></a></li>

                   <li class=\"facebook\" style=\"display: block;\">
                       <iframe src=\"//www.facebook.com/plugins/like.php?href=http://www.sainsburys.co.uk/shop/gb/groceries/sainsburys-avocado-xl-pinkerton-loose-300g&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21\" scrolling=\"no\" frameborder=\"0\" allowtransparency=\"true\"></iframe>
                   </li>

           </ul>
        </div>


        <div class=\"tabs\">

            <ul class=\"tabLinks\">
                <li class=\"first currentTab\">
                    <a href=\"#information\" class=\"currentTab\" onclick=\"s_objectID=&quot;http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado-x_3&quot;;return this.s_oc?this.s_oc(e):true\">Information</a>
                </li>

            </ul>



            <div class=\"section\" id=\"information\">
                <h2 class=\"access\">Information</h2>
                <productcontent xmlns:a=\"http://www.inspire-js.com/SOL\">
<htmlcontent contentpath=\"/Content/media/html/products/label//_label_inspire.html\" outputmethod=\"xhtml\">
<h3 class=\"productDataItemHeader\">Description</h3>
<div class=\"productText\">
<p>Avocados</p>
<p>
</p><p></p>
<p></p>
</div>

<h3 class=\"productDataItemHeader\">Nutrition</h3>
<div class=\"productText\">
<div>
<p>
<strong>Table of Nutritional Information</strong>
</p>
<div class=\"tableWrapper\">
<table class=\"nutritionTable\">
<thead>
<tr class=\"tableTitleRow\">
<th scope=\"col\">Typical Values</th><th scope=\"col\">Per 100g&nbsp;</th><th scope=\"col\">% based on RI for Average Adult</th>
</tr>
</thead>
<tbody><tr class=\"tableRow1\">
<th scope=\"row\" class=\"rowHeader\" rowspan=\"2\">Energy</th><td class=\"\">813kJ</td><td class=\"\">-</td>
</tr>
<tr class=\"tableRow0\">
<td class=\"\">198kcal</td><td class=\"\">10%</td>
</tr>
<tr class=\"tableRow1\">
<th scope=\"row\" class=\"rowHeader\">Fat</th><td class=\"tableRow1\">19.5g</td><td class=\"tableRow1\">28%</td>
</tr>
<tr class=\"tableRow0\">
<th scope=\"row\" class=\"rowHeader\">Saturates</th><td class=\"tableRow0\">4.1g</td><td class=\"tableRow0\">21%</td>
</tr>
<tr class=\"tableRow1\">
<th scope=\"row\" class=\"rowHeader\">Mono unsaturates</th><td class=\"tableRow1\">12.1g</td><td class=\"tableRow1\">-</td>
</tr>
<tr class=\"tableRow0\">
<th scope=\"row\" class=\"rowHeader\">Polyunsaturates</th><td class=\"tableRow0\">2.2g</td><td class=\"tableRow0\">-</td>
</tr>
<tr class=\"tableRow1\">
<th scope=\"row\" class=\"rowHeader\">Carbohydrate</th><td class=\"tableRow1\">1.9g</td><td class=\"tableRow1\">1%</td>
</tr>
<tr class=\"tableRow0\">
<th scope=\"row\" class=\"rowHeader\">Total Sugars</th><td class=\"tableRow0\">&lt;0.5g</td><td class=\"tableRow0\">-</td>
</tr>
<tr class=\"tableRow1\">
<th scope=\"row\" class=\"rowHeader\">Fibre</th><td class=\"tableRow1\">3.4g</td><td class=\"tableRow1\">-</td>
</tr>
<tr class=\"tableRow0\">
<th scope=\"row\" class=\"rowHeader\">Protein</th><td class=\"tableRow0\">1.9g</td><td class=\"tableRow0\">4%</td>
</tr>
<tr class=\"tableRow1\">
<th scope=\"row\" class=\"rowHeader\">Salt</th><td class=\"tableRow1\">&lt;0.5g</td><td class=\"tableRow1\">-</td>
</tr>
</tbody></table>
</div>
<p>RI= Reference Intakes of an average adult (8400kJ / 2000kcal)</p>
</div>
</div>

<h3 class=\"productDataItemHeader\">Ingredients</h3>
<div class=\"productText\">
<p>Varieties Hass, Pinkerton and Ryan
</p>
</div>

<h3 class=\"productDataItemHeader\">Size</h3>
<div class=\"productText\">
<p>275g</p>
</div>

<h3 class=\"productDataItemHeader\">Packaging</h3>
<div class=\"productText\">
<p>Other plastic label -glued</p>
</div>

<h3 class=\"productDataItemHeader\">Manufacturer</h3>
<div class=\"productText\">
<p>We are happy to replace this item if it is not satisfactory</p>
<p>Sainsbury's Supermarkets Ltd.</p>
<p>33 Holborn, London EC1N 2HT</p>
<p>Customer services 0800 636262</p>
</div>

</htmlcontent>
</productcontent>

                <p></p><h3>Important Information</h3><p>The above details have been prepared to help you select suitable products. Products and their ingredients are liable to change.</p><p><strong>You should always read the label before consuming or using the product and never rely solely on the information presented here.</strong></p><p>If you require specific advice on any Sainsbury's branded product, please contact our Customer Careline on 0800 636262. For all other products, please contact the manufacturer.</p><p>
This information is supplied for your personal use only. It may not be reproduced in any way without the prior consent of Sainsbury's Supermarkets Ltd and due acknowledgement.</p><p></p>
            </div>


        </div>

            <p class=\"skuCode\">7678882</p>

    </div>
</div>
<div id=\"additionalItems_572163\" class=\"additionalProductInfo\">

      <!--  Left hand side column --><!-- BEGIN MerchandisingAssociationsDisplay.jsp --><!-- Start - JSP File Name:  MerchandisingAssociationsDisplay.jsp --><!-- END MerchandisingAssociationsDisplay.jsp -->

    <div class=\"badges\">
        <ul>




                     <li>



                        <img src=\"http://www.sainsburys.co.uk/wcsstore7.11.1.161/SainsburysStorefrontAssetStore/wcassets/icons/ico_spacer.gif\" alt=\"Vegetarian\">


                    </li>




                     <li class=\"lastchild\">



                        <img src=\"http://www.sainsburys.co.uk/wcsstore7.11.1.161/SainsburysStorefrontAssetStore/wcassets/icons/ico_spacer.gif\" alt=\"Vegan\">


                    </li>

        </ul>
    </div>


  </div>

<!-- END CachedProductOnlyDisplay.jsp -->
          </div>";
    }

}
